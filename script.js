console.log("test")


/*

Mini-Activity:
 	1. Create an "activity" folder, an "index.html" file inside of it and link the "script.js" file.
	2. Create a "script.js" file and console log the message "Hello World" to ensure that the script file is properly associated with the html file.
	3. Create a "trainer" object by using object literals.
	4. Initialize/add the "trainer" object properties.
	5. Initialize/add the "trainer" object method.
	6. Access the "trainer" object properties using dot and square bracket notation.
	7 .Invoke/call the "trainer" object method.
	8. Create a constructor function for creating a pokemon.
	9. Create/instantiate several pokemon using the "constructor" function.
	10. Have the pokemon objects interact with each other by calling on the "tackle" method.

	*/


function trainer(name){
	this.name = name;
	this.health = 100;
	this.train = function(){
		console.log(`${this.name} is created`);
	};
	this.attack = function(target){
		console.log(`${this.name} attacked ${target.name}`);
		target.health -= 50;
		console.log(`${this.name}'s health is now ${target.health}`);
	}
};

let Pickachu = new trainer("Pickachu");
let Geodude = new trainer("Geodude");

Pickachu.train();
Geodude.train();

Pickachu.attack(Geodude);







// function trainer(name){
// 	this.name = name;
// 	this.health = 100;
// 	this.train = function(){
// 		console.log(`${this.name} is created`);
// 	};

// };

// let Pickachu = new trainer("Pickachu");
// let Geodude = new trainer("Geodude");

// Pickachu.train();
// Geodude.train();

// function Pokemon(name)
// 	this.name = name;
// 	this.health = 100;
// 		this.attack = function(target){
// 		console.log(`${this.name} attacked ${target.name}`);
// 		target.health -= 50;
// 		console.log(`${this.name}'s health is now ${target.health}`);
// 	}

// Pickachu.attack(Geodude);









